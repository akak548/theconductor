import os
import json


class ConductorConfig(object):

    def __init__(self, config_file=None):
        if config_file:
            self.config_file = config_file
        else:
            self.config_file = os.path.expanduser('~') + '/.conducter.json'

    @property
    def default_id(self):
        try:
            self.__config_data['default']
        except Exception:
            return '1'

    def __config_data(self):
        try:
            with open(self.config_file) as cfile:
                return json.load(cfile)
        except EnvironmentError:
            return dict()

    def add_profile(self, conductor_id, account_id, region, default=False):
        config = self.__config_data()

        try:
            accounts = config.get('account', dict())
            accounts[account_id] = {}
            accounts[account_id]['region'] = region

            config['account'] = accounts

            with open(self.config_file, 'w') as myfile:
                json.dump(config, myfile)

        except IOError, e:
            raise Exception(e)

    def set_default(self, profile):
        pass
