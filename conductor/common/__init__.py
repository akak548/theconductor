class AWSCredentials:

    def __init__(self):
        self.home_directoy = os.path.expanduser('~')

    @classmethod
    def get_accounts(cls):
        accounts = []
        with open(cls.home_directoy + '/.aws/credentials') as aws_file:
            for line in aws_file.readlines():
                if re.match('^\[.*\]$', line):
                    accounts.append(line[1:-2])
        return accounts

    def get_valid_regions(self):
        return ["us-east-1", "us-west-1", "us-west-2"]