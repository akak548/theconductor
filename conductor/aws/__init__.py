import re
import os
import commands
import inspect

home_directoy = os.path.expanduser('~')

class AWS(object):
    @property
    def accounts(self):
        accounts = []
        with open(home_directoy + '/.aws/credentials') as aws_file:
            for line in aws_file.readlines():
                if re.match('^\[.*\]$', line):
                    accounts.append(line[1:-2])
            return accounts

    @property
    def regions(self):
        return ["us-east-1", "us-west-1", "us-west-2"]


def handlers():
    cement_handlers = list()

    for name, obj in inspect.getmembers(commands):
        if inspect.isclass(obj):
            cement_handlers.append(obj)
    return cement_handlers
