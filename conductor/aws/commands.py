import conductor
import setup

global_argument = [(['--region'], dict(help="another option under base controller"))]


class AWSController(conductor.CementBaseController):
    class Meta:
        label = 'aws'
        description = "Main aws command"
        stacked_on = 'base'
        stacked_type = 'nested'
        arguments = global_argument


class AWSAccountController(conductor.CementBaseController):
    class Meta:
        label = 'aws_account'
        description = 'list all available users'
        aliases = ['account']
        aliases_only = True
        stacked_on = 'aws'
        stacked_type = 'nested'


class AWSCMDController(conductor.CementBaseController):
    class Meta:
        label = 'aws_cmd'
        description = 'aws command base'
        aliases = ['command']
        aliases_only = True
        stacked_on = 'aws'
        stacked_type = 'nested'


class AWSAccountSetupController(conductor.CementBaseController):
    class Meta:
        label = 'aws_acct_setup'
        description = 'setup aws account'
        aliases = ['setup']
        aliases_only = True
        stacked_on = 'aws_account'
        stacked_type = 'nested'

    @conductor.expose()
    def default(self):
        setup.Account()


class AWSCMDSetupController(conductor.CementBaseController):
    class Meta:
        label = 'aws_cmd_setup'
        description = 'setup ec2 command'
        aliases = ['setup']
        aliases_only = True
        stacked_on = 'aws_cmd'
        stacked_type = 'nested'

    @conductor.expose()
    def default(self):
        setup.Command()
