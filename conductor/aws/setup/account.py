import npyscreen
from conductor.config import ConductorConfig
import conductor.aws

class AWSAccountForm(npyscreen.ActionForm):

    def __init(self, *args, **keywords):
        super(AWSAccountForm, self).__init__(*args, **keywords)

    def afterEditing(self):
        self.parentApp.setNextForm(None)

    def create(self):
        self.conducter_id = self.add(npyscreen.TitleText, max_width=100, max_height=2, value=self.parentApp.config.default_id, name="ConducterName: ",)
        self.aws_account = self.add(npyscreen.TitleSelectOne, max_height=len(self.parentApp.aws.accounts) + 1, name="AWS Accounts",
            values=self.parentApp.aws.accounts, scroll_exit=True)
        self.region = self.add(npyscreen.TitleMultiSelect, max_height=len(self.parentApp.aws.regions) + 1, value=[], name="AWS Region",
                values=self.parentApp.aws.regions, scroll_exit=True)
        self.default = self.add(npyscreen.TitleMultiSelect, max_height=-2, value=[], name="Set as default",
                values=['True'], scroll_exit=True)

    def on_cancel(self):
        myfile = open('nofile.txt', 'w+')
        myfile.write('Error')
        myfile.close()

    def on_ok(self):
        account = self.parentApp.aws.accounts[self.aws_account.value[0]]
        region = [self.parentApp.aws.regions[select_region] for select_region in self.region.value]

        self.parentApp.config.add_profile(self.conducter_id.value, account, region, True)

class AWSAccountFormConfiguration(npyscreen.NPSAppManaged):
    def onStart(self):
        self.config = ConductorConfig()
        self.aws = conductor.aws.AWS()
        self.addForm('MAIN', AWSAccountForm, name='Conductor AWS Account')

class Account():
    def __init__(self):
        app = AWSAccountFormConfiguration()
        app.run()
