from cement.core.controller import CementBaseController, expose
from cement.core.foundation import CementApp
from conductor.aws import handlers as aws_handlers

version = '0.0.1'


class BaseController(CementBaseController):
    class Meta:
        label = 'base'


def handers():
    handlers = [BaseController]
    return handlers + aws_handlers()

