try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup
import os

requires = [
    'cement==1.1.1',
    'boto3==4.0.0',
    'npyscreen==',
]
setup(
    name='theconductor',
    version='0.0.1',
    description="A CLI tool for AWS EC2 Run command for automation",
    long_description=open('README.md').read(),
    author='Cornell Greene',
    author_email='akak548@gmail.com',
    url='',
    install_requires=requires,
    scripts='bin/conductor',
    license=open('LICENSE').read(),
    classifiers=(
        'Development Status :: 2 - Pre-Alpha',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        'Natural Language :: English',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
    )

)