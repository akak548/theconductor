Overview
========
A commannd line program written in Python that allows user to utilize ec2 run command to control ec2 instance


Sample Commands
========
conductor aws setup
conductor aws setup cmd
conductor aws setup account

conductor aws run <cmd> <env> <product> <role>
conductor aws run puppet all
conductor aws run puppet --region us-west-1 --name dev-cws-web-01
conductor aws run puppet --region us-west-1,us-east-1 --env dev --product cws --role web
conductor aws run puppet --region us-west-1 --env dev --product cws --role web

conductor aws list cmd
conductor aws list account

conducer ssh run 'yum update'

Sample Configuration File
----
{
    'default': <conducter name>
    'conductor_name': {
        'aws_account': {

        },
        'aws_region': 'us-west-1, us-east-1'
    }
}
